package com.hjdb88;

import org.bouncycastle.crypto.AsymmetricCipherKeyPair;
import org.bouncycastle.crypto.params.ECPrivateKeyParameters;
import org.bouncycastle.crypto.params.ECPublicKeyParameters;
import org.bouncycastle.util.encoders.Hex;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.math.BigInteger;
import java.util.Arrays;

public class Sm2UtilTest extends BaseTest {
    @Test
    public void testSignAndVerify() {
        try {
            AsymmetricCipherKeyPair keyPair = Sm2Util.generateKeyPair();
            ECPrivateKeyParameters priKey = (ECPrivateKeyParameters) keyPair.getPrivate();
            ECPublicKeyParameters pubKey = (ECPublicKeyParameters) keyPair.getPublic();

            byte[] sign = Sm2Util.sign(USER_ID, priKey, DATA);
            System.out.println("SM2 sign with withId result:\n" + Arrays.toString(sign));
            boolean flag = Sm2Util.verify(USER_ID, pubKey, DATA, sign);
            if (!flag) {
                Assert.assertTrue(false);
            }

            sign = Sm2Util.sign(priKey, DATA);
            System.out.println("SM2 sign without withId result:\n" + Arrays.toString(sign));
            flag = Sm2Util.verify(pubKey, DATA, sign);
            if (!flag) {
                Assert.assertTrue(false);
            }
            Assert.assertTrue(true);
        } catch (Exception ex) {
            ex.printStackTrace();
            Assert.assertTrue(false);
        }
    }

    @Test
    public void testEncryptAndDecrypt() {
        try {
            AsymmetricCipherKeyPair keyPair = Sm2Util.generateKeyPair();
            ECPrivateKeyParameters priKey = (ECPrivateKeyParameters) keyPair.getPrivate();
            ECPublicKeyParameters pubKey = (ECPublicKeyParameters) keyPair.getPublic();

            byte[] encryptedData = Sm2Util.encryt(pubKey, DATA);
            System.out.println("SM2 encrypt result:\n" + Arrays.toString(encryptedData));
            byte[] decryptedData = Sm2Util.decrypt(priKey, encryptedData);
            System.out.println("SM2 decrypt result:\n" + Arrays.toString(decryptedData));
            if (!Arrays.equals(decryptedData, DATA)) {
                Assert.assertTrue(false);
            }
            Assert.assertTrue(true);
        } catch (Exception ex) {
            ex.printStackTrace();
            Assert.assertTrue(false);
        }
    }

    @Test
    public void testKeyPairEncoding() {
        try {
            AsymmetricCipherKeyPair keyPair = Sm2Util.generateKeyPair();
            ECPrivateKeyParameters priKey = (ECPrivateKeyParameters) keyPair.getPrivate();
            ECPublicKeyParameters pubKey = (ECPublicKeyParameters) keyPair.getPublic();

            byte[] publicKeyByte = new byte[64];
            byte[] privateKeyByte = priKey.getD().toByteArray();
            System.arraycopy(pubKey.getQ().getEncoded(), 1, publicKeyByte, 0, 64);

            String publicKey = Hex.toHexString(publicKeyByte);
            String privateKey = Hex.toHexString(privateKeyByte);
            System.out.println("私钥: " + privateKey);
            System.out.println("公钥: " + publicKey);

            ECPrivateKeyParameters newPriKey = Sm2Util.getPrivateKey(new BigInteger(Hex.decode(privateKey)));
            ECPublicKeyParameters newPubKey = Sm2Util.getPublicKey(Hex.decode("04" + publicKey));

            byte[] priKeyPkcs8Der = Sm2Util.convertEcPriKeyToPkcs8Der(priKey, pubKey);
            System.out.println("private key pkcs8 der length:" + priKeyPkcs8Der.length);
            System.out.println("private key pkcs8 der:" + Arrays.toString(priKeyPkcs8Der));
            writeFile("E:/cert/ec.pkcs8.pri", priKeyPkcs8Der);
            ECPrivateKeyParameters newPriKey2 = Sm2Util.convertPkcs1DerToEcPriKey(priKeyPkcs8Der);

            byte[] priKeyPkcs1Der = Sm2Util.convertEcPriKeyToPkcs1Der(priKey, pubKey);
            System.out.println("private key pkcs1 der length:" + priKeyPkcs1Der.length);
            System.out.println("private key pkcs1 der:" + Arrays.toString(priKeyPkcs1Der));
            writeFile("E:/cert/ec.pkcs1.pri", priKeyPkcs1Der);

            byte[] pubKeyX509Der = Sm2Util.convertEcPubKeyToX509Der(pubKey);
            System.out.println("public key der length:" + pubKeyX509Der.length);
            System.out.println("public key der:" + Arrays.toString(pubKeyX509Der));
            writeFile("E:/cert/ec.x509.pub", pubKeyX509Der);

            Assert.assertTrue(true);
        } catch (Exception ex) {
            ex.printStackTrace();
            Assert.assertTrue(false);
        }
    }

    private void writeFile(String filePath, byte[] data) throws IOException {
        RandomAccessFile raf = null;
        try {
            raf = new RandomAccessFile(filePath, "rw");
            raf.write(data);
        } finally {
            if (raf != null) {
                raf.close();
            }
        }
    }
}