package com.hjdb88;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

public class Sm3UtilTest extends BaseTest {
    @Test
    public void testHashAndVerify() {
        try {
            byte[] hash = Sm3Util.hash(DATA);
            System.out.println("SM3 hash result:\n" + Arrays.toString(hash));
            boolean flag = Sm3Util.verify(DATA, hash);
            if (!flag) {
                Assert.assertTrue(false);
            }
            Assert.assertTrue(true);
        } catch (Exception ex) {
            ex.printStackTrace();
            Assert.assertTrue(false);
        }
    }

    @Test
    public void testHmacSm3() {
        try {
            byte[] hmacKey = new byte[]{1, 2, 3, 4, 5, 6, 7, 8};
            byte[] hmac = Sm3Util.hmac(hmacKey, DATA);
            System.out.println("SM3 hash result:\n" + Arrays.toString(hmac));
            Assert.assertTrue(true);
        } catch (Exception ex) {
            ex.printStackTrace();
            Assert.assertTrue(false);
        }
    }
}