package com.hjdb88;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

public class Sm4UtilTest extends BaseTest {

    @Test
    public void testEncryptAndDecrypt() {
        try {
            byte[] key = Sm4Util.generateKey();
            byte[] iv = Sm4Util.generateKey();
            byte[] cipherText;
            byte[] decryptedData;

            cipherText = Sm4Util.encrypt_Ecb_Padding(key, DATA);
            System.out.println("SM4 ECB Padding encrypt result:\n" + Arrays.toString(cipherText));
            decryptedData = Sm4Util.decrypt_Ecb_Padding(key, cipherText);
            System.out.println("SM4 ECB Padding decrypt result:\n" + Arrays.toString(decryptedData));
            if (!Arrays.equals(decryptedData, DATA)) {
                Assert.assertTrue(false);
            }

            cipherText = Sm4Util.encrypt_Cbc_Padding(key, iv, DATA);
            System.out.println("SM4 CBC Padding encrypt result:\n" + Arrays.toString(cipherText));
            decryptedData = Sm4Util.decrypt_Cbc_Padding(key, iv, cipherText);
            System.out.println("SM4 CBC Padding decrypt result:\n" + Arrays.toString(decryptedData));
            if (!Arrays.equals(decryptedData, DATA)) {
                Assert.assertTrue(false);
            }

            Assert.assertTrue(true);
        } catch (Exception ex) {
            ex.printStackTrace();
            Assert.assertTrue(false);
        }
    }
}