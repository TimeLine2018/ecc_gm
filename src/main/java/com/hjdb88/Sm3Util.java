package com.hjdb88;

import org.bouncycastle.crypto.digests.SM3Digest;
import org.bouncycastle.crypto.macs.HMac;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import java.security.Security;
import java.util.Arrays;

/**
 * SM3工具类
 *
 * @author hjdb88
 */
public class Sm3Util {
    static {
        Security.addProvider(new BouncyCastleProvider());
    }

    private Sm3Util() {
    }

    /**
     * 哈希
     *
     * @param data 源数据
     * @return
     */
    public static byte[] hash(byte[] data) {
        SM3Digest digest = new SM3Digest();
        digest.update(data, 0, data.length);
        byte[] hash = new byte[digest.getDigestSize()];
        digest.doFinal(hash, 0);
        return hash;
    }

    /**
     * 验证哈希值
     *
     * @param data
     * @param sm3Hash
     * @return
     */
    public static boolean verify(byte[] data, byte[] sm3Hash) {
        byte[] newHash = hash(data);
        if (Arrays.equals(newHash, sm3Hash)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * hmac
     *
     * @param key  key
     * @param data 源数据
     * @return
     */
    public static byte[] hmac(byte[] key, byte[] data) {
        KeyParameter keyParameter = new KeyParameter(key);
        SM3Digest digest = new SM3Digest();
        HMac mac = new HMac(digest);
        mac.init(keyParameter);
        mac.update(data, 0, data.length);
        byte[] result = new byte[mac.getMacSize()];
        mac.doFinal(result, 0);
        return result;
    }
}